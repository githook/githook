# Hooks

The hooks directory is for any third party hook repos or any hook files.

Each hooks should be in it's own file whether it's a cli script or some third party hook.

Then in `hooks.php`, include it in the `chain_hook`. Look at the `hooks.example.php` to see an example.
