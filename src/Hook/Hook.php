<?php

namespace Githook\Hook;

use Psr\Log\LoggerInterface,
    Psr\Log\NullLogger;

/** interface for Hooks which take HookInfo and do anything with it */
interface Hook {
    public function __invoke(HookInfo $hook);
}

function is_hook($hook) {
    return $hook instanceof Hook || is_callable($hook);
}

function hook_execute($hook, HookInfo $info) {
    if (!is_hook($hook)) {
        throw new \InvalidArgumentException('Invalid type of hook of supplied');
    }

    $hook($info);
}

/** creates a chain hook */
function chain_hook($hooks) {
    return function(HookInfo $info) use ($hooks) {
        foreach ($hooks as $hook) {
            hook_execute($hook, $info);
        }
    };
}

/** do nothing */
function null_hook() {
    return function(HookInfo $info) {};
}

/** creates a filtering hook */
function filter_hook($filter, $hook) {
    return function (HookInfo $info) use ($filter, $hook) {
        if ($filter($info)) {
            hook_execute($hook, $info);
        }
    };
}

/** maps the hook data before applying */
function map_hook($map, $hook) {
    return function(HookInfo $info) use ($map, $hook) {
        hook_execute($hook, $map($info));
    };
}

/** creates a hook that writes to a cli command */
function cli_hook($command, LoggerInterface $log = null) {
    $log = $log ?: new NullLogger();
    return function(HookInfo $info) use ($command, $log) {
        $log->debug('CLI Hook', ['command' => $command]);

        $spec = [
            ['pipe', 'r'],
            ['pipe', 'w']
        ];

        $pipes = [];
        $process = proc_open($command, $spec, $pipes);

        if (!is_resource($process)) {
            $log->error('CLI Hook - process was not a resource');
            return;
        }

        fwrite($pipes[0], json_encode($info));
        fclose($pipes[0]);

        $output = stream_get_contents($pipes[1]);
        fclose($pipes[1]);

        $log->debug("CLI Hook Output:\n".$output);
    };
}
