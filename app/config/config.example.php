<?php

/**
 * An exmample githook config file
 */

use Githook\GithookConfig,
    Githook\Hook;

return new GithookConfig(
    /** Create a HookController instance */
    new Hook\Controller\GitlabHookController(),
    /** Load the hooks */
    require __DIR__ . '/../../hooks/hooks.php'
);
