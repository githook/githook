ifndef port
    port=8000
endif

.PHONY: setup composer clean server

server:
	php -S 0.0.0.0:$(port) web/index.php

setup: hooks/hooks.php app/config/config.php app/logs composer

composer: composer.phar
	php composer.phar install --no-dev

clean:
	rm -rf app/config/config.php hooks/hooks.php app/logs composer.phar

hooks/hooks.php:
	cp hooks/hooks.example.php hooks/hooks.php

app/config/config.php:
	cp app/config/config.example.php app/config/config.php

app/logs:
	mkdir -p app/logs; chmod 777 app/logs

composer.phar:
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php -r "if (hash_file('SHA384', 'composer-setup.php') === '92102166af5abdb03f49ce52a40591073a7b859a86e8ff13338cf7db58a19f7844fbc0bb79b2773bf30791e935dbd938') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	php composer-setup.php
	php -r "unlink('composer-setup.php');"
