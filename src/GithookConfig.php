<?php

namespace GitHook;

/**
 * Class for holding app configuration
 */
class GithookConfig
{
    public $controller;
    public $hook;

    public function __construct(Hook\Controller\HookController $controller, $hook) {
        $this->controller = $controller;
        $this->hook = $hook;
    }

    public static function createFromFile($path) {
        $config = require $path;

        if (!$config instanceof GithookConfig) {
            throw new \LogicException(sprintf(
                'config file at %s did not return a GithookConfig instance',
                $path
            ));
        }

        return $config;
    }
}
