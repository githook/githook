<?php

namespace Githook;

use Silex\Application,
    Silex\Api\ControllerProviderInterface;

class GithookControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app) {
        $routes = $app['controllers_factory'];

        $routes->post('/receive', 'githook.controller.receive:handleHookAction');

        return $routes;
    }
}
