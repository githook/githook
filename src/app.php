<?php

namespace Githook;

use Monolog,
    Silex;

function app() {
    static $app = null;

    if ($app) {
        return $app;
    }

    $app = app_bootstrap(new Silex\Application());

    return $app;
}

function app_bootstrap(Silex\Application $app) {
    $app->register(new GithookServiceProvider());
    $app->register(new Silex\Provider\MonologServiceProvider(), [
        'monolog.name' => 'Githook',
        'monolog.logfile' => $app['githook.paths.log'] . '/githook.log',
    ]);
    $app->extend('monolog.formatter', function($formatter) {
        return new Monolog\Formatter\LineFormatter(null, null, true);
    });

    $app->register(new Silex\Provider\ServiceControllerServiceProvider());
    $app->mount('', new GithookControllerProvider());

    return $app;
}
