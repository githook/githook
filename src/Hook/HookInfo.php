<?php

namespace Githook\Hook;

/** Simple structure for holding hook info */
class HookInfo
{
    /** @var string */
    public $provider;
    /** @var string */
    public $type;
    /** @var array */
    public $data;

    public function __construct($provider, $type, $data) {
        $this->provider = $provider;
        $this->type = $type;
        $this->data = $data;
    }
}
