<?php

namespace Githook;

use Pimple\Container,
    Pimple\ServiceProviderInterface;

class GithookServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app) {
        $app['githook.paths.app'] = __DIR__ . '/../app';
        $app['githook.paths.config'] = $app['githook.paths.app'] . '/config';
        $app['githook.paths.log'] = $app['githook.paths.app'] . '/logs';

        $app['githook.config'] = function($app) {
            return GithookConfig::createFromFile($app['githook.paths.config'] . '/config.php');
        };

        $app['githook.controller.receive'] = function($app) {
            $config = $app['githook.config'];
            return new Controller\ReceiveController(
                $config->controller,
                $config->hook,
                $app['monolog']
            );
        };
    }
}
