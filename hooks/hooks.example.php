<?php

use Githook\Hook;

return hook\chain_hook([
    hook\cli_hook('cat', githook\app()['logger']),
    require __DIR__ . '/third-party-hook.php'
]);
