<?php

namespace Githook\Hook;

/** filter for branches */
function branch_filter($branch_re) {
    return function(HookInfo $info) use ($branch_re) {
        $prefix = 'refs/heads/';
        $len = strlen($prefix);

        return $info->type == 'push' &&
            strpos($info->data['ref'], $prefix) === 0 &&
            preg_match($branch_re, substr($info->data['ref'], $len));
    };
}

function not_filter($filter) {
    return function(HookInfo $info) use ($filter) {
        return !$filter($info);
    };
}

function not_branch_filter($branch_re) {
    return not_filter(branch_filter($branch_re));
}
