<?php

namespace Githook\Hook\Controller;

use Githook\Hook\HookInfo,
    Symfony\Component\HttpFoundation\Request;

class GitlabHookController implements HookController
{
    public function handleRequest(Request $req) {
        if (!$req->headers->has('X-Gitlab-Event')) {
            return;
        }

        $data = json_decode($req->getContent(), true);

        if (!$data || !array_key_exists('object_kind', $data)) {
            return;
        }

        return new HookInfo('gitlab', $data['object_kind'], $data);
    }
}
