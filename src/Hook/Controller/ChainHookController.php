<?php

namespace Githook\Hook\Controller;

use Githook\Hook\HookInfo,
    Symfony\Component\HttpFoundation\Request;

class ChainHookController implements HookController
{
    private $controllers;

    public function __construct($controllers) {
        $this->controllers = $controllers;
    }

    public function handleRequest(Request $req) {
        foreach ($this->controllers as $controller) {
            $res = $controller->handleRequest($req);
            if ($res) { return $res; }
        }
    }
}
