<?php

namespace Githook\Controller;

use GitHook\Hook,
    GitHook\Model\CliInputFactory,

    Psr\Log\LoggerInterface,

    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

/**
 * Receive Controller
 * Responsible for handling the actual hooks
 */
class ReceiveController
{
    private $controller;
    private $hook;
    private $logger;

    public function __construct(
        Hook\Controller\HookController $controller,
        $hook,
        LoggerInterface $logger
    ) {
        $this->controller = $controller;
        $this->hook = $hook;
        $this->logger = $logger;
    }

    public function handleHookAction(Request $req)
    {
        $this->logger->debug('ReceiveController::handleHookAction');

        $info = $this->controller->handleRequest($req);

        if (!$info) {
            $this->logger->info('no handler was found');
            return new Response('no handler found');
        }

        try {
            hook\hook_execute($this->hook, $info);
        } catch (\Exception $e) {
            $this->logger->error('An exception was thrown', ['e' => $e]);
            return new Response('error');
        }

        $this->logger->debug('ReceiveController::handleHookAction end');
        return new Response('success');
    }
}
