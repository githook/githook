<?php

require_once __DIR__ . '/../vendor/autoload.php';

$app = githook\app();

if (getenv('GITHOOK_DEBUG')) {
    // register debug settings
    $app['debug'] = true;
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    Symfony\Component\Debug\ErrorHandler::register();
}

$app->run();
