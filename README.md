# Githook

GitHook is a application that listens and handles Gitlab web hooks. It's designed to be a simple app that listens for web hooks and then uses a configurable hook system to actual do the processing.

## Installation

```
git clone git@gitlab.bighead.net:bighead/githook.git
make setup
make server port=8000
```

then your host will then be able to receive web hooks at `http://{host}:8000/receive`. The system won't do anything until you configure it to do something with the hooks and which controller is to handle the requests.

## Configuration

Any configuration is handled in `app/config/config.php`.

The config file returns an instance of `Githook\GithookConfig`. You specify the Hook controller and the Hook itself.

## Design

The application listens for post requests on `/receive`. The config has a `HookController` and a `Hook`. The HookController is responsible for taking a request and returning a `HookInfo` structure. The only HookController right now is for Gitlab. The Hook is responsible for processing the HookInfo.

When the app receives a post request, it gets the HookInfo from the configured controller. If found, then it passes the HookInfo into the configured hook. That's it! The rest of the power of the system lies in the Hooks and creating any custom hook for your needs.

## Logs

You can find the log file in `app/logs`
