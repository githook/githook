# Hook

The hook module holds all of the hook system related code. Currently, there are `Hook`'s and `HookController`'s.

## HookController

A hook controller is responsible for transforming a request into a HookInfo. If the request can't be properly verified, then null is returned.

## Hook

A hook is a callable that accepts the `HookInfo` struct. A hook can literally do anything they want. Because of their simple interface, they are very composable which allows decoration.
