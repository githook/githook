<?php

namespace Githook\Hook\Controller;

use Githook\Hook\HookInfo,
    Symfony\Component\HttpFoundation\Request;

interface HookController {
    /** takes a request and returns a hook info. Returns null if hook info isn't
        found.
        @return HookInfo
    */
    public function handleRequest(Request $req);
}
